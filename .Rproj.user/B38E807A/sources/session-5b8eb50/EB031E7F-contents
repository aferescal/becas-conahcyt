---
title: "Análisis exploratorio - Becas al extranjero CONAHCYT"
author: "Angel F. Escalante"
format: 
  html:
    theme: spacelab
    fontsize: 1rem
editor: visual
---

## Introducción

En el ámbito de la educación y la investigación, las becas representan un recurso fundamental para el desarrollo académico y científico de una nación. En México, el Consejo Nacional de Ciencia y Tecnología (CONACYT) desempeña un papel crucial al proporcionar becas a estudiantes y profesionales que buscan ampliar sus horizontes educativos y de investigación a nivel nacional e internacional. Estas becas, especialmente las otorgadas para estudios en el extranjero, han sido un pilar fundamental para el crecimiento del capital humano en el país y su participación en la comunidad global de académicos e investigadores.

El presente análisis exploratorio de datos tiene como objetivo examinar la evolución de las becas al extranjero otorgadas por el Consejo Nacional de Ciencia y Tecnología (CONACYT) en México desde el año 2015 hasta el presente, 2023. Se ha suscitado un debate en torno a la reducción significativa en el número de becas otorgadas para estudios en el extranjero durante este período, y el presente informe se propone corroborar esta afirmación a través del análisis de datos públicos disponibles.

La educación y la investigación son motores esenciales para el progreso de una nación, y las becas juegan un papel crítico al proporcionar a los estudiantes y profesionales la oportunidad de acceder a una formación de calidad en instituciones internacionales de renombre. Comprender la tendencia en la otorgación de becas al extranjero por parte de CONACYT es fundamental para evaluar el compromiso del país con la formación de capital humano de alta calidad y su participación en la comunidad académica y científica global. En este contexto, este análisis exploratorio de datos se presenta como una herramienta para arrojar luz sobre la evolución de las becas al extranjero en México y proporcionar una base sólida para discutir los posibles impactos de estas tendencias en la educación y la investigación en el país.

## Fuente de información

La información consiste en archivos de Excel desde el 2015 al 2022 completos y 2023 parcialmente (hasta junio). Se pueden descargar del sitio [oficial](https://conahcyt.mx/becas_posgrados/padron-de-beneficiarios/).

Cargando los paquetes para la exploración:

```{r message=FALSE, warning=FALSE}
library(tidyverse)
library(scales)
library(janitor)
library(readxl)
library(here)
```

## Procesamiento de datos

Establecer ubicación de los archivos

```{r}
lista_archivos <- list.files(here("docs", "datos"), full.names = TRUE)
```

Leyendo archivos

```{r warning=FALSE, message=FALSE}
archivos_xlsx <- 
  2015:2022 |> 
  map(
    ~ read_excel(grep(.x, lista_archivos, value = TRUE)) |> clean_names() |> 
      mutate(anio = .x)
  ) |> set_names(2015:2022)
```

Seleccionando columnas en común...

```{r}
interseccion_columnas <- 
  archivos_xlsx |> 
  map(names) |> 
  reduce(intersect)

data_final <- archivos_xlsx |> 
  map(\(data) select(data, all_of(interseccion_columnas))) |> 
  reduce(bind_rows) |> 
  arrange(anio, consec) |> 
  rename(id = consec) |>
  mutate(id = 1:n()) |> 
  relocate(anio)
data_final
```

## Exploratorio

Buscando encontrar gráficos que sean de utilidad, veamos que nos dicen los datos:

```{r}
#| fig-width: 10
#| fig-height: 8
theme_set(theme_minimal())

data_final |> 
  ggplot(aes(x = as.factor(anio), y = total_pagado)) + 
  geom_boxplot() +
  facet_wrap(~ nivel_de_estudios) + 
  scale_y_continuous(labels = unit_format(unit = "M", scale = 1e-6)) +
  labs(x = "Año", y = "Total pagado") +
  theme(axis.text.x = element_text(angle = 45, hjust = 1))
```

```{r}
#| fig-width: 10
#| fig-height: 8
data_final |> 
  ggplot(aes(x = as.factor(anio), y = total_pagado)) + 
  geom_boxplot() +
  facet_wrap(~ area_del_conocimiento) + 
  scale_y_continuous(labels = unit_format(unit = "M", scale = 1e-6)) +
  labs(x = "Año", y = "Total pagado") +
  theme(axis.text.x = element_text(angle = 45, hjust = 1))
```

Requerimos más limpieza:

* ¿Qué significa `N/A`? ¿Por qué hay tanto dinero asignado a estos conceptos?
* Hay etiquetas distintas tanto para `Nivel de estudios` como para `Area de conocimiento`, necesitamos estandarizárlos.
* Hay presencia de outliers, hay que identificarlos y determinar si debemos conservarlos o eliminarlos.

